#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		printf("[+] syntax : add-nbo <file1> <file2>\n");
		printf("[+] example : add-nbo a.bin b.bin\n");
		return 1;
	}

	FILE* fileA = fopen(argv[1], "r");
	FILE* fileB = fopen(argv[2], "r");

	if(fileA == 0){
		printf("[-] File Not Exist. (%s)\n",argv[1]);
		return 2;
	}
	if(fileB == 0){
		printf("[-] File Not Exist. (%s)\n",argv[2]);
		return 3;
	}

	char Abuf[5] = {0,0,0,0,0};
	int countA = fread(Abuf,1,5,fileA);
	if(countA != 4)
	{
		printf("[-] Wrong File format. File must have 4 byte.\n[-] %s have %d byte(s).\n",argv[1],countA);
		return 4;
	}

	char Bbuf[5] = {0,0,0,0,0};
	int countB = fread(Bbuf,1,5,fileB);
	if(countB != 4)
	{
		printf("[-] Wrong File format. File must have 4 byte.\n[-] %s have %d byte(s).\n",argv[2],countB);
		return 5;
	}

	uint32_t* Aptr = (uint32_t*)Abuf;
	uint32_t* Bptr = (uint32_t*)Bbuf;
	uint32_t Aval = ntohl(*Aptr);
	uint32_t Bval = ntohl(*Bptr);
	uint32_t Rval = Aval + Bval;

	printf("%d(0x%x) + %d(0x%x) = %d(0x%x)\n",Aval,Aval,Bval,Bval,Rval,Rval);
	
	return 0;
}
